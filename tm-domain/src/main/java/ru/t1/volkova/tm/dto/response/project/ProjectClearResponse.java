package ru.t1.volkova.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.response.AbstractResponse;
import ru.t1.volkova.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ProjectClearResponse extends AbstractResponse {

    @Nullable
    private List<Project> projects;

    public ProjectClearResponse(@Nullable final List<Project> projects) {
        this.projects = projects;
    }

}
