package ru.t1.volkova.tm.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.ISessionRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.Session;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 3;

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private List<String> userIdList;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final SqlSession sqlSession = connectionService.getSqlSession();

    @NotNull
    final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);

    @Before
    public void initRepository() throws SQLException {
        sessionList = new ArrayList<>();
        userIdList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull Session session = new Session();
            session.setRole(Role.USUAL);
            @NotNull String userId = UUID.randomUUID().toString();
            userIdList.add(userId);
            session.setUserId(userId);
            sessionRepository.add(session);
            sessionList.add(session);
        }
    }

    @Test
    public void testAddSession() throws SQLException {
        @NotNull final Random random = new Random();
        @NotNull final String userId = userIdList.get(random.nextInt(userIdList.size()));
        int expectedNumberOfEntries = sessionRepository.getSize(userId) + 1;
        @NotNull final Session newSession = new Session();
        newSession.setUserId(userId);
        sessionRepository.add(newSession);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(userId));
    }

    @Test
    public void testFindAllForUser() throws SQLException {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(2);
        @Nullable final List<Session> sessionList = sessionRepository.findAll(userId1);
        @Nullable final List<Session> sessionList2 = sessionRepository.findAll(userId2);
        if (sessionList != null && sessionList2 != null) {
            Assert.assertEquals(sessionRepository.getSize(userId1), sessionList.size());
            Assert.assertEquals(sessionRepository.getSize(userId2), sessionList2.size());
        }
    }

    @Test
    public void testFindAllForUserNegative() throws SQLException {
        @Nullable final List<Session> sessionList = sessionRepository.findAll((String) null);
        Assert.assertEquals(0, sessionList.size());
        @Nullable final List<Session> sessionList2 = sessionRepository.findAll("non-existent-id");
        if (sessionList2 == null) return;
        Assert.assertEquals(0, sessionList2.size());
    }

    @Test
    public void testFindOneByIdForUser() throws SQLException {
        for (@NotNull final Session session : sessionList) {
            assertEquals(session, sessionRepository.findOneById(session.getUserId(), session.getId()));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() throws SQLException {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(2);
        Assert.assertNull(sessionRepository.findOneById(null, sessionList.get(1).getId()));
        Assert.assertNull(sessionRepository.findOneById(userId1, "NotExcitingId"));
        Assert.assertNull(sessionRepository.findOneById(userId2, "NotExcitingId"));
    }

    @Test
    public void testFindOneByIndexForUser() throws Exception {
        @NotNull final Random random = new Random();
        @NotNull final String userId = userIdList.get(random.nextInt(userIdList.size()));
        @Nullable final Session session = sessionRepository.findOneByIndex(userId, 0);
        Assert.assertNotNull(session);
    }

    @Test
    public void testFindOneByIndexForUserNegative() throws Exception {
        @NotNull final Random random = new Random();
        @NotNull final String userId = userIdList.get(random.nextInt(userIdList.size()));
        Assert.assertNull(sessionRepository.findOneByIndex(userId, NUMBER_OF_ENTRIES + 10));
    }

    @Test
    public void testRemoveOneForUser() throws Exception {
        @NotNull final String userId1 = userIdList.get(0);
        @Nullable final Session session1 = sessionRepository.findOneByIndex(userId1, 0);
        sessionRepository.removeOne(session1);
        Assert.assertEquals(0, sessionRepository.getSize(userId1));
    }

    @Test
    public void testRemoveOneByIdForUser() throws SQLException {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String id = sessionList.get(0).getId();
        sessionRepository.removeOneById(userId1, id);
        Assert.assertEquals(0, sessionRepository.getSize(userId1));
    }

    @Test
    public void testRemoveAllForUser() throws SQLException {
        @NotNull final String userId1 = userIdList.get(0);
        @NotNull final String userId2 = userIdList.get(1);
        sessionRepository.removeAll(userId1);
        sessionRepository.removeAll(userId2);
        Assert.assertEquals(0, sessionRepository.getSize(userId1));
        Assert.assertEquals(0, sessionRepository.getSize(userId2));
    }

    @Test
    public void testRemoveAllForUserNegative() throws SQLException {
        @NotNull final String userId1 = userIdList.get(2);
        @NotNull final String userId2 = userIdList.get(1);
        sessionRepository.removeAll("NotExcitingId");
        sessionRepository.removeAll((String) null);
        Assert.assertNotEquals(0, sessionRepository.getSize(userId1));
        Assert.assertNotEquals(0, sessionRepository.getSize(userId2));
    }

    @Test
    public void testRemoveOneByIndexForUser() throws Exception {
        @NotNull final Random random = new Random();
        @NotNull final String userId = userIdList.get(random.nextInt(userIdList.size()));
        System.out.println(sessionRepository.getSize(userId));
        sessionRepository.removeOneByIndex(userId, 0);
        Assert.assertEquals(0, sessionRepository.getSize(userId));
    }

    @Test
    public void testGetSizeForUser() throws SQLException {
        @NotNull final String userId1 = userIdList.get(2);
        @NotNull final String userId2 = userIdList.get(1);
        Assert.assertNotEquals(0, sessionRepository.getSize(userId1));
        Assert.assertNotEquals(0, sessionRepository.getSize(userId2));
    }

}
