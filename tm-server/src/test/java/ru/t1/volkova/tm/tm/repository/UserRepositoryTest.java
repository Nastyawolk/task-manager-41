package ru.t1.volkova.tm.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.ITaskRepository;
import ru.t1.volkova.tm.api.repository.IUserRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertNull;

public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 4;

    @NotNull
    private List<User> userList;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final SqlSession sqlSession = connectionService.getSqlSession();

    @NotNull
    final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);

    @Before
    public void initRepository() throws SQLException {
        userList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull User user = new User();
            user.setLogin("user" + i);
            user.setEmail("user@" + i + ".ru");
            user.setRole(Role.USUAL);
            userRepository.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testAddUser() throws SQLException {
        int expectedNumberOfEntries = userRepository.getSize() + 1;
        @NotNull final User newUser = new User();
        userRepository.add(newUser);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testFindAll() throws Exception {
        @Nullable final List<User> userList = userRepository.findAll();
        Assert.assertNotEquals(0, userList.size());
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final User expected1 = userList.get(1);
        @NotNull final User expected2 = userList.get(2);
        Assert.assertEquals(expected1, userRepository.findOneById(expected1.getId()));
        Assert.assertEquals(expected2, userRepository.findOneById(expected2.getId()));
    }

    @Test
    public void testFindOneByIdNegative() throws Exception {
        Assert.assertNull(userRepository.findOneById("NotExcitingId"));
    }

    @Test
    public void testRemoveOne() throws Exception {
        @Nullable final User user = userRepository.findOneById(userList.get(0).getId());
        userRepository.removeOne(user);
        assertNull(userRepository.findOneById(userList.get(0).getId()));
    }

    @Test
    public void testRemoveAll() throws SQLException {
        userRepository.removeAll();
        Assert.assertEquals(0, userRepository.getSize());
    }


    @Test
    public void testGetSize() throws SQLException {
        Assert.assertNotEquals(0, userRepository.getSize());
    }

    @Test
    public void testFindByLogin() throws SQLException {
        Assert.assertEquals(userList.get(1), userRepository.findOneByLogin("user2"));
        Assert.assertEquals(userList.get(0), userRepository.findOneByLogin("user1"));
    }

    @Test
    public void testFindByLoginNegative() throws SQLException {
        Assert.assertNull(userRepository.findOneByLogin("user-test"));
    }

    @Test
    public void testFindByEmail() throws SQLException {
        Assert.assertEquals(userList.get(1), userRepository.findOneByEmail("user@2.ru"));
        Assert.assertEquals(userList.get(0), userRepository.findOneByEmail("user@1.ru"));
    }

    @Test
    public void testFindByEmailNegative() throws SQLException {
        Assert.assertNull(userRepository.findOneByEmail("test@ru"));
    }


}
