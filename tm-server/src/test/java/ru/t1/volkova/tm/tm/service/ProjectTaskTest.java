package ru.t1.volkova.tm.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.service.*;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.volkova.tm.exception.field.TaskIdEmptyException;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.Task;
import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.service.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Random;

public class ProjectTaskTest {

    private static String USER_ID_1;

    private static String USER_ID_2;

    @Nullable
    private List<Project> projectList;

    @Nullable
    private List<Task> taskList;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, projectService, taskService, propertyService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService, connectionService);

    @Before
    public void initRepository() throws SQLException {
        projectList = projectService.findAlls();
        taskList = taskService.findAlls();
        @Nullable final User user = userService.findByLogin("user");
        @Nullable final User userTest = userService.findByLogin("test");
        if (user == null || userTest == null || projectList == null) return;
        USER_ID_1 = user.getId();
        USER_ID_2 = userTest.getId();
    }

    @Test
    public void testBindTaskToProject() throws Exception {
        @NotNull final Random random = new Random();
        @Nullable List<Project> userProjects = projectService.findAll(USER_ID_2);
        @Nullable List<Task> userTasks = taskService.findAll(USER_ID_2);
        if (userProjects == null || userTasks == null) return;
        final int projectIndex = random.nextInt(userProjects.size());
        final int taskIndex = random.nextInt(userTasks.size());
        @NotNull final String projectId = userProjects.get(projectIndex).getId();
        @NotNull final String taskId = userTasks.get(taskIndex).getId();
        @NotNull final Task bindedTask = projectTaskService.bindTaskToProject(USER_ID_2, projectId, taskId);
        Assert.assertEquals(projectId, bindedTask.getProjectId());
        Assert.assertEquals(taskId, bindedTask.getId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testBindTaskWithEmptyProjectId() throws Exception {
        @NotNull final Random random = new Random();
        @Nullable List<Task> userTasks = taskService.findAll(USER_ID_1);
        if (userTasks == null) return;
        final int taskIndex = random.nextInt(userTasks.size());
        @NotNull final String taskId = userTasks.get(taskIndex).getId();
        projectTaskService.bindTaskToProject(USER_ID_1, null, taskId);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testBindTaskWithEmptyTaskId() throws Exception {
        @NotNull final Random random = new Random();
        @Nullable List<Project> userProjects = projectService.findAll(USER_ID_1);
        if (userProjects == null) return;
        final int projectIndex = random.nextInt(userProjects.size());
        @NotNull final String projectId = userProjects.get(projectIndex).getId();
        projectTaskService.bindTaskToProject(USER_ID_1, projectId, null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testBindTaskWithTaskNotFound() throws Exception {
        if (projectList == null) return;
        @NotNull final String projectId = projectList.get(0).getId();
        projectTaskService.bindTaskToProject(USER_ID_1, projectId, "non-existent");
    }

    @Test
    public void testRemoveByProjectId() throws SQLException {
        if (taskList == null) return;
        @Nullable String projectId = null;
        @Nullable String userId = null;
        for (@NotNull final Task task : taskList) {
            if (task.getProjectId() == null) continue;
            projectId = task.getProjectId();
            userId = task.getUserId();
            break;
        }
        if (projectId == null && userId == null) return;
        int projectSize = projectService.getSize(userId);
        projectTaskService.removeProjectById(userId, projectId);
        @Nullable final List<Task> tasks = taskService.findAllByProjectId(userId, projectId);
        if (tasks == null) return;
        Assert.assertEquals(0, tasks.size());
        Assert.assertNotEquals(projectSize, projectService.getSize(userId));
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testRemoveByEmptyProjectId() throws SQLException {
        projectTaskService.removeProjectById(USER_ID_1, null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testRemoveByNonExistProjectId() throws SQLException {
        projectTaskService.removeProjectById(USER_ID_1, "NonExist");
    }

    @Test(expected = TaskNotFoundException.class)
    public void testRemoveByProjectIdTaskException() throws SQLException {
        @Nullable final Project project = new Project();
        project.setUserId(USER_ID_1);
        projectService.add(project);
        projectTaskService.removeProjectById(USER_ID_1, project.getId());
    }

    @Test
    public void testUnbindTaskToProject() throws SQLException {
        if (taskList == null) return;
        @Nullable String projectId = null;
        @Nullable String userId = null;
        @Nullable String taskId = null;
        for (@NotNull final Task task : taskList) {
            if (task.getProjectId() == null) continue;
            projectId = task.getProjectId();
            userId = task.getUserId();
            taskId = task.getId();
            break;
        }
        if (projectId == null && userId == null) return;
        projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        @Nullable Task task = taskService.findOneById(userId, taskId);
        if (task == null) return;
        Assert.assertNull(task.getProjectId());
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void testUnbindTaskWithEmptyProjectId() throws SQLException {
        @NotNull final Random random = new Random();
        @Nullable List<Task> userTasks = taskService.findAll(USER_ID_1);
        if (userTasks == null) return;
        final int taskIndex = random.nextInt(userTasks.size());
        @NotNull final String taskId = userTasks.get(taskIndex).getId();
        projectTaskService.unbindTaskFromProject(USER_ID_1, null, taskId);
    }

    @Test(expected = TaskIdEmptyException.class)
    public void testUnbindTaskWithEmptyTaskId() throws SQLException {
        @NotNull final Random random = new Random();
        @Nullable List<Project> userProjects = projectService.findAll(USER_ID_1);
        if (userProjects == null) return;
        final int projectIndex = random.nextInt(userProjects.size());
        @NotNull final String projectId = userProjects.get(projectIndex).getId();
        projectTaskService.unbindTaskFromProject(USER_ID_1, projectId, null);
    }

    @Test(expected = TaskNotFoundException.class)
    public void testUnbindTaskWithTaskNotFound() throws SQLException {
        @NotNull final Random random = new Random();
        @Nullable List<Project> userProjects = projectService.findAll(USER_ID_1);
        if (userProjects == null) return;
        final int projectIndex = random.nextInt(userProjects.size());
        @NotNull final String projectId = userProjects.get(projectIndex).getId();
        projectTaskService.unbindTaskFromProject(USER_ID_1, projectId, "non-existent");
    }

}
