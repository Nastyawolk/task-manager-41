package ru.t1.volkova.tm.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.IProjectRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class ProjectRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final SqlSession sqlSession = connectionService.getSqlSession();

    @NotNull
    final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);

    @Before
    public void initRepository() throws SQLException {
        projectList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull Project project = new Project();
            project.setName("Rproject" + i);
            project.setDescription("description" + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectRepository.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testAddForUserId() throws SQLException {
        int expectedNumberOfEntries = NUMBER_OF_ENTRIES / 2 + 1;
        @NotNull final String name = "Test project";
        @NotNull final String description = "Test description";
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_ID_1));
    }

    @Test
    public void testFindAll() throws SQLException {
        @Nullable final List<Project> projectList = projectRepository.findAlls();
    }

    @Test
    public void testFindAllForUser() throws SQLException {
        @Nullable final List<Project> projectList = projectRepository.findAll(USER_ID_1);
        @Nullable final List<Project> projectList2 = projectRepository.findAll(USER_ID_2);
        if (projectList != null && projectList2 != null) {
            assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectList.size());
            assertEquals(NUMBER_OF_ENTRIES / 2 - 1, projectList2.size());
        }
    }

    @Test
    public void testFindAllForUserNegative() throws SQLException {
        @Nullable final List<Project> projectList = projectRepository.findAll((String) null);
        assertEquals(0, projectList.size());
        @Nullable final List<Project> projectList2 = projectRepository.findAll("non-existent-id");
        if (projectList2 == null) return;
        assertEquals(0, projectList2.size());
    }

    @Test
    public void testFindOneByIdForUser() throws SQLException {
        for (@NotNull final Project project : projectList) {
            assertEquals(project, projectRepository.findOneById(project.getUserId(), project.getId()));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() throws SQLException {
        Assert.assertNull(projectRepository.findOneById(null, projectList.get(1).getId()));
        Assert.assertNull(projectRepository.findOneById(USER_ID_1, "NotExcitingId"));
        Assert.assertNull(projectRepository.findOneById(USER_ID_2, "NotExcitingId"));
    }

    @Test
    public void testFindOneByIndexForUser() throws Exception {
        @Nullable final Project project1 = projectRepository.findOneByIndex(USER_ID_1, 1);
        @NotNull final Project expected1 = projectList.get(1);
        assertEquals(expected1, project1);
    }

    @Test
    public void testFindOneByIndexForUserNegative() throws Exception {
        Assert.assertNull(projectRepository.findOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES + 10));
    }

    @Test
    public void testRemoveOneForUser() throws Exception {
        @Nullable final Project project1 = projectRepository.findOneById(USER_ID_1, projectList.get(0).getId());
        projectRepository.removeOne(project1);
        assertNull(projectRepository.findOneById(USER_ID_1, projectList.get(0).getId()));
    }

    @Test
    public void testRemoveOneByIdForUser() throws SQLException {
        for (int i = 0; i < NUMBER_OF_ENTRIES / 2 + 1; i++) {
            @NotNull final Project project1 = projectList.get(i);
            projectRepository.removeOneById(USER_ID_1, project1.getId());
            assertNull(projectRepository.findOneById(USER_ID_1, project1.getId()));
        }
        for (int i = NUMBER_OF_ENTRIES / 2 + 1; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project2 = projectList.get(i);
            projectRepository.removeOneById(USER_ID_2, project2.getId());
            assertNull(projectRepository.findOneById(USER_ID_2, project2.getId()));
        }
    }

    @Test
    public void testRemoveAllForUser() throws SQLException {
        projectRepository.removeAll(USER_ID_1);
        projectRepository.removeAll(USER_ID_2);
        assertEquals(0, projectRepository.getSize(USER_ID_1));
        assertEquals(0, projectRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveAllForUserNegative() throws SQLException {
        projectRepository.removeAll("NotExcitingId");
        projectRepository.removeAll((String) null);
        Assert.assertNotEquals(0, projectRepository.getSize(USER_ID_1));
        Assert.assertNotEquals(0, projectRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveOneByIndexForUser() throws Exception {
        @Nullable final Project project1 = projectRepository.findOneByIndex(USER_ID_1, 1);
        projectRepository.removeOneByIndex(USER_ID_1, 1);
        assertEquals(NUMBER_OF_ENTRIES / 2, projectRepository.getSize(USER_ID_1));
        if (project1 != null) Assert.assertNull(projectRepository.findOneById(USER_ID_1, project1.getId()));
    }

    @Test
    public void testGetSizeForUser() throws SQLException {
        assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectRepository.getSize(USER_ID_1));
        assertEquals(NUMBER_OF_ENTRIES / 2 - 1, projectRepository.getSize(USER_ID_2));
        assertEquals(projectList.size() / 2 + 1, projectRepository.getSize(USER_ID_1));
        assertEquals(projectList.size() / 2 - 1, projectRepository.getSize(USER_ID_2));
    }

}
