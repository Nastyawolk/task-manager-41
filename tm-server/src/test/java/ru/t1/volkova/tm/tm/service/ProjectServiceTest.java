package ru.t1.volkova.tm.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.IProjectService;
import ru.t1.volkova.tm.api.service.ITaskService;
import ru.t1.volkova.tm.api.service.IUserService;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.field.DescriptionEmptyException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.IndexIncorrectException;
import ru.t1.volkova.tm.exception.field.NameEmptyException;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.service.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Random;

public class ProjectServiceTest {

    private static String USER_ID;

    private static String USER_TEST_ID;

    @Nullable
    private List<Project> projectList;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService userService = new UserService(connectionService, projectService, taskService, propertyService);

    @Before
    public void initRepository() throws SQLException {
        projectList = projectService.findAlls();
        @Nullable final User user = userService.findByLogin("user");
        @Nullable final User userTest = userService.findByLogin("test");
        if (user == null || userTest == null || projectList == null) return;
        USER_ID = user.getId();
        USER_TEST_ID = userTest.getId();
    }

    @Test
    public void testCreate(
    ) throws Exception {
        @Nullable final Project project = projectService.create(USER_ID, "new_project", "new description");
        if (project == null) return;
        Assert.assertEquals(project, projectService.findOneById(USER_ID, project.getId()));
    }

    @Test
    public void testCreateNegative(
    ) throws SQLException {
        @Nullable final Project project = projectService.create(null, "new_project", "new description");
        Assert.assertNull(project);
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateNameEmpty(
    ) throws SQLException {
        projectService.create(USER_ID, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateDescriptionEmpty(
    ) throws SQLException {
        projectService.create(USER_TEST_ID, "new", null);
    }

    @Test
    public void testUpdateById() throws SQLException {
        @NotNull final String newName = "new project";
        @NotNull final String newDescription = "new project";
        if (projectList == null) return;
        @Nullable final Project project = projectList.get(0);
        if (project == null) return;
        projectService.updateById(project.getUserId(), project.getId(), newName, newDescription);
        @Nullable final Project newProject = projectService.findOneById(project.getUserId(), project.getId());
        if (newProject == null) return;
        Assert.assertEquals(newName, newProject.getName());
        Assert.assertEquals(newDescription, newProject.getDescription());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateNotFoundProject(
    ) throws SQLException {
        @NotNull final String newName = "new project";
        @NotNull final String newDescription = "new project";
        @NotNull final String id = "non-existent-id";
        projectService.updateById(USER_ID, id, newName, newDescription);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateIdEmpty(
    ) throws SQLException {
        projectService.updateById(USER_ID, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmpty(
    ) throws SQLException {
        projectService.updateById(USER_ID, projectList.get(0).getId(), null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmpty(
    ) throws SQLException {
        projectService.updateById(USER_TEST_ID, projectList.get(0).getId(), "name", null);
    }

    @Test
    public void testUpdateByIndex() throws SQLException {
        @NotNull final String newName = "new project";
        @NotNull final String newDescription = "new project";
        @NotNull final Random random = new Random();
        final int index = random.nextInt(projectService.getSize(USER_ID));
        @Nullable final Project project = projectService.updateByIndex(USER_ID, index, newName, newDescription);
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateIdEmptyByIndex(
    ) throws SQLException {
        projectService.updateByIndex(USER_ID, 10, "name", "new description");
        projectService.updateByIndex(USER_ID, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmptyByIndex(
    ) throws SQLException {
        projectService.updateByIndex(USER_ID, 2, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmptyByIndex(
    ) throws SQLException {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(projectService.getSize(USER_ID));
        projectService.updateByIndex(USER_ID, index, "name", null);
    }

    @Test
    public void testChangeStatusById() throws SQLException {
        @NotNull final Project projectToUpdate = projectList.get(0);
        @Nullable final Project project = projectService.changeProjectStatusById(projectToUpdate.getUserId(), projectToUpdate.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeStatusIdEmptyById() throws SQLException {
        projectService.changeProjectStatusById(USER_ID, null, Status.IN_PROGRESS);
        projectService.changeProjectStatusById(USER_ID, "", Status.IN_PROGRESS);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeStatusProjectNotFoundById() throws SQLException {
        projectService.changeProjectStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        projectService.changeProjectStatusById(USER_ID, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testChangeStatusByIndex() throws SQLException {
        @Nullable final Project project = projectService.changeProjectStatusByIndex(USER_ID, 2, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusIndexExceptionByIndex() throws SQLException {
        projectService.changeProjectStatusByIndex(USER_ID, 22, Status.IN_PROGRESS);
        projectService.changeProjectStatusByIndex(USER_ID, null, Status.IN_PROGRESS);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeStatusProjectNotFoundByIndex() throws SQLException {
        projectService.changeProjectStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        projectService.changeProjectStatusById(USER_ID, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testFindOneById() throws SQLException {
        @NotNull final Random random = new Random();
        final int index = random.nextInt(projectService.getSize(USER_ID));
        final int index2 = random.nextInt(projectService.getSize(USER_ID));
        Assert.assertNotNull(projectService.findOneById(USER_ID, projectList.get(index).getId()));
        Assert.assertNotNull(projectService.findOneById(USER_TEST_ID, projectList.get(index2).getId()));
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testFindOneByIdNegative() throws SQLException {
        Assert.assertNotNull(projectService.findOneById(USER_ID, "non-existent"));
    }

}
