package ru.t1.volkova.tm.tm.repository;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.repository.IProjectRepository;
import ru.t1.volkova.tm.api.repository.ITaskRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.Task;
import ru.t1.volkova.tm.service.ConnectionService;
import ru.t1.volkova.tm.service.PropertyService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 6;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Task> taskList;

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final SqlSession sqlSession = connectionService.getSqlSession();

    @NotNull
    final ITaskRepository taskRepository = sqlSession.getMapper(ITaskRepository.class);

    @NotNull
    final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);

    @Before
    public void initRepository() throws SQLException {
        taskList = new ArrayList<>();
        @NotNull Project project = new Project();
        @NotNull Project project2 = new Project();
        project.setUserId(USER_ID_1);
        project2.setUserId(USER_ID_2);
        projectRepository.add(project);
        projectRepository.add(project2);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull Task task = new Task();
            task.setName("task" + i);
            task.setDescription("Tdescription" + i);
            if (i <= 3) {
                task.setUserId(USER_ID_1);
                task.setProjectId(project.getId());
            } else {
                task.setUserId(USER_ID_2);
                task.setProjectId(project2.getId());
            }
            taskRepository.add(task);
            taskList.add(task);
        }
    }

    @Test
    public void testAddForUserId() throws SQLException {
        int expectedNumberOfEntries = taskRepository.getSize(USER_ID_1) + 1;
        @NotNull final String name = "Test task";
        @NotNull final String description = "Test description";
        @NotNull Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(USER_ID_1);
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER_ID_1));
        @Nullable final Task actualTask = taskRepository.findOneById(USER_ID_1, task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(name, actualTask.getName());
        Assert.assertEquals(description, actualTask.getDescription());
        Assert.assertEquals(USER_ID_1, actualTask.getUserId());
    }

    @Test
    public void testFindAllForUser() throws SQLException {
        @Nullable final List<Task> taskList = taskRepository.findAll(USER_ID_1);
        @Nullable final List<Task> taskList2 = taskRepository.findAll(USER_ID_2);
        if (taskList != null && taskList2 != null) {
            Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskList.size());
            Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, taskList2.size());
        }
    }

    @Test
    public void testFindAllForUserNegative() throws SQLException {
        @Nullable final List<Task> taskList = taskRepository.findAll((String) null);
        Assert.assertEquals(0, taskList.size());
        @Nullable final List<Task> taskList2 = taskRepository.findAll("non-existent-id");
        if (taskList2 == null) return;
        Assert.assertEquals(0, taskList2.size());
    }

    @Test
    public void testFindOneByIdForUser() throws SQLException {
        for (int i = 0; i < NUMBER_OF_ENTRIES / 2 + 1; i++) {
            @NotNull final String id = taskList.get(i).getId();
            Assert.assertEquals(taskList.get(i), taskRepository.findOneById(USER_ID_1, id));
        }
        for (int i = NUMBER_OF_ENTRIES / 2 + 1; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final String id = taskList.get(i).getId();
            Assert.assertEquals(taskList.get(i), taskRepository.findOneById(USER_ID_2, id));
        }
    }

    @Test
    public void testFindOneByIdForUserNegative() throws SQLException {
        Assert.assertNull(taskRepository.findOneById(null, taskList.get(1).getId()));
        Assert.assertNull(taskRepository.findOneById(USER_ID_1, "NotExcitingId"));
        Assert.assertNull(taskRepository.findOneById(USER_ID_2, "NotExcitingId"));
    }

    @Test
    public void testFindOneByIndexForUser() throws Exception {
        @Nullable final Task task1 = taskRepository.findOneByIndex(USER_ID_1, 1);
        @NotNull final Task expected1 = taskList.get(1);
        Assert.assertEquals(expected1, task1);
    }

    @Test
    public void testFindOneByIndexForUserNegative() throws Exception {
        Assert.assertNull(taskRepository.findOneByIndex(USER_ID_1, NUMBER_OF_ENTRIES + 20));
    }

    @Test
    public void testRemoveOneForUser() throws Exception {
        @Nullable final Task task = taskRepository.findOneById(USER_ID_1, taskList.get(0).getId());
        if (task == null) return;
        taskRepository.removeOne(task);
        assertNull(taskRepository.findOneById(USER_ID_1, taskList.get(0).getId()));
    }

    @Test
    public void testRemoveOneByIdForUser() throws SQLException {
        for (int i = 0; i < NUMBER_OF_ENTRIES / 2 + 1; i++) {
            @NotNull final Task task = taskList.get(i);
            taskRepository.removeOneById(USER_ID_1, task.getId());
            assertNull(taskRepository.findOneById(USER_ID_1, task.getId()));
        }
        for (int i = NUMBER_OF_ENTRIES / 2 + 1; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = taskList.get(i);
            taskRepository.removeOneById(USER_ID_2, task.getId());
            assertNull(taskRepository.findOneById(USER_ID_2, task.getId()));
        }
    }

    @Test
    public void testRemoveAllForUser() throws SQLException {
        taskRepository.removeAll(USER_ID_1);
        taskRepository.removeAll(USER_ID_2);
        Assert.assertEquals(0, taskRepository.getSize(USER_ID_1));
        Assert.assertEquals(0, taskRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveAllForUserNegative() throws SQLException {
        taskRepository.removeAll("NotExcitingId");
        taskRepository.removeAll((String) null);
        Assert.assertNotEquals(0, taskRepository.getSize(USER_ID_1));
        Assert.assertNotEquals(0, taskRepository.getSize(USER_ID_2));
    }

    @Test
    public void testRemoveOneByIndexForUser() throws Exception {
        @Nullable final Task task = taskRepository.findOneByIndex(USER_ID_1, 1);
        taskRepository.removeOneByIndex(USER_ID_1, 1);
        assertEquals(NUMBER_OF_ENTRIES / 2, taskRepository.getSize(USER_ID_1));
        if (task != null) Assert.assertNull(taskRepository.findOneById(USER_ID_1, task.getId()));

    }

    @Test
    public void testGetSizeForUser() throws SQLException {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskRepository.getSize(USER_ID_1));
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 - 1, taskRepository.getSize(USER_ID_2));
        Assert.assertEquals(taskList.size() / 2 + 1, taskRepository.getSize(USER_ID_1));
        Assert.assertEquals(taskList.size() / 2 - 1, taskRepository.getSize(USER_ID_2));
    }

    @Test
    public void findAllByProjectId() throws Exception {
        @NotNull final Project project = projectRepository.findAll(USER_ID_1).get(0);
        @Nullable final List<Task> tasks = taskRepository.findAllByProjectId(USER_ID_1, project.getId());
        Assert.assertNotNull(tasks);
    }

    @Test
    public void findAllByProjectIdNegative() throws Exception {
        @NotNull final Project project = projectRepository.findAll(USER_ID_1).get(0);
        @Nullable final List<Task> tasks = taskRepository.findAllByProjectId(null, project.getId());
        if (tasks == null) return;
        Assert.assertEquals(0, tasks.size());
    }

}
