package ru.t1.volkova.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.IUserRepository;
import ru.t1.volkova.tm.api.service.*;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.entity.UserNotFoundException;
import ru.t1.volkova.tm.exception.field.*;
import ru.t1.volkova.tm.exception.user.EmailExistsException;
import ru.t1.volkova.tm.exception.user.LoginExistsException;
import ru.t1.volkova.tm.exception.user.RoleEmptyException;
import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.util.HashUtil;

import java.sql.SQLException;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IConnectionService connectionService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService,
            @NotNull final IPropertyService propertyService
    ) {
        this.connectionService = connectionService;
        this.taskService = taskService;
        this.projectService = projectService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        user.setEmail(email);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        user.setRole(role);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.add(user);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @Nullable
    @Override
    public List<User> findAll() throws SQLException {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findAll();
        }
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findOneByLogin(login);
        }
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) throws SQLException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findOneByEmail(email);
        }
    }

    @Override
    @NotNull
    public User findOneById(@Nullable final String id) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = repository.findOneById(id);
            if (user == null) throw new ProjectNotFoundException();
            return user;
        }
    }

    @NotNull
    @Override
    public User removeOne(@Nullable final User user) throws Exception {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        if (user == null) throw new UserNotFoundException();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            repository.removeOne(user);
            @NotNull final String userId = user.getId();
            taskService.removeAll(userId);
            projectService.removeAll(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        return this.removeOne(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) throws Exception {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        return this.removeOne(user);
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            repository.update(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            repository.update(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) return false;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findOneByLogin(login) != null;
        }
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) throws SQLException {
        if (email == null || email.isEmpty()) return false;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.findOneByEmail(email) != null;
        }
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            repository.update(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
            repository.update(user);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int getSize() throws SQLException {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository repository = sqlSession.getMapper(IUserRepository.class);
            return repository.getSize();
        }
    }

}
