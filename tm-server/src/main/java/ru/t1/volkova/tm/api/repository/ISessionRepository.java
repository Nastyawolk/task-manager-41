package ru.t1.volkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.Session;

import java.sql.SQLException;
import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, date, user_id, role)" +
            " VALUES (#{id}, #{date}, #{userId}, #{role})")
    int add(@NotNull Session session) throws SQLException;

    @Select("SELECT * FROM tm_session WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    Session findOneById(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("id") String id
    ) throws SQLException;

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    Session findOneByIndex(
            @Nullable @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    ) throws SQLException;

    @Select("SELECT COUNT(*) FROM tm_session WHERE user_id = #{userId}")
    int getSize(@Nullable @Param("userId") String userId
    ) throws SQLException;

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable
    List<Session> findAll(@Nullable @Param("userId") String userId) throws SQLException;

    @Delete("DELETE FROM tm_session WHERE id = #{id} AND user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void removeOneById(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("id") String id
    ) throws SQLException;

    @Delete("DELETE FROM tm_session WHERE id = (SELECT id from tm_session WHERE " +
            "user_id = #{userId} LIMIT 1 OFFSET #{index})")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void removeOneByIndex(
            @Nullable @Param("userId") String userId,
            @NotNull @Param("index") Integer index
    ) throws SQLException;

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void removeOne(@NotNull Session session) throws SQLException;

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    void removeAll(@Nullable @Param("userId") String userId
    ) throws SQLException;

}
