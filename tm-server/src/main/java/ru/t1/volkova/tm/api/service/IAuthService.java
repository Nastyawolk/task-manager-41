package ru.t1.volkova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.Session;
import ru.t1.volkova.tm.model.User;

import java.sql.SQLException;

public interface IAuthService {

    @NotNull
    User registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email) throws SQLException;

    String login(
            @Nullable String login,
            @Nullable String password
    ) throws SQLException;

    @SneakyThrows
    Session validateToken(@Nullable String token);

    void invalidate(Session session) throws Exception;

}
