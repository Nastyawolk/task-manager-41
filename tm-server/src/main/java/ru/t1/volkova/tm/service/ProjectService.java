package ru.t1.volkova.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.IProjectRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.IProjectService;
import ru.t1.volkova.tm.comparator.CreatedComparator;
import ru.t1.volkova.tm.comparator.NameComparator;
import ru.t1.volkova.tm.comparator.StatusComparator;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.field.*;
import ru.t1.volkova.tm.model.Project;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    private final IConnectionService connectionService;

    public ProjectService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Nullable
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description) throws SQLException {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.add(project);
            sqlSession.commit();
        } catch (@NotNull Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
        } catch (@NotNull Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws SQLException {
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
        } catch (@NotNull Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        project.setStatus(status);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
        } catch (@NotNull Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws SQLException {
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        project.setStatus(status);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.update(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            @Nullable final Project project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        }
    }

    @NotNull
    @Override
    public Project findOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            @Nullable final Project project = repository.findOneByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId,
                                 @Nullable final Comparator comparator
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            if (comparator == CreatedComparator.INSTANSE) return projectRepository.findAllByCreated(userId);
            if (comparator == StatusComparator.INSTANSE) return projectRepository.findAllByStatus(userId);
            if (comparator == NameComparator.INSTANSE) return projectRepository.findAllByName(userId);
            return findAll(userId);
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAll(userId);
        }
    }

    @Nullable
    @Override
    public List<Project> findAlls() throws SQLException {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            return projectRepository.findAlls();
        }
    }

    @Override
    @Nullable
    public List<Project> findAllByName(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findAllByName(userId);
        }
    }

    @Override
    @Nullable
    public List<Project> findAllByStatus(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findAllByStatus(userId);
        }
    }

    @Override
    @Nullable
    public List<Project> findAllByCreated(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.findAllByCreated(userId);
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            return repository.getSize(userId);
        }
    }

    @Override
    @Nullable
    public Project removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Project project;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneById(userId, id);
            repository.removeOneById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Override
    @Nullable
    public Project removeOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Project project;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            project = repository.findOneByIndex(userId, index);
            repository.removeOneByIndex(userId, index);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Override
    @NotNull
    public Project removeOne(
            @Nullable final String userId,
            @NotNull final Project project
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.removeOne(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

    @Override
    public void removeAll(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository repository = sqlSession.getMapper(IProjectRepository.class);
            repository.removeAll(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @NotNull
    public Project add(
            @NotNull final Project project
    ) throws SQLException {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
            projectRepository.add(project);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return project;
    }

}
