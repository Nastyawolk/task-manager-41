package ru.t1.volkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.AbstractModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @Nullable
    M add(@NotNull M model) throws SQLException;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws SQLException;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull Integer index) throws Exception;

    @Nullable
    M removeOne(@Nullable M model) throws Exception;

    @Nullable
    M removeOneById(@NotNull String id) throws Exception;

    @Nullable
    M removeOneByIndex(@Nullable Integer index) throws Exception;

    void removeAll() throws SQLException;

    void removeAll(@Nullable List<M> models) throws SQLException;

    @NotNull
    Integer getSize() throws SQLException;

}
