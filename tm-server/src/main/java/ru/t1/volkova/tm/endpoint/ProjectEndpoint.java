package ru.t1.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.volkova.tm.api.service.IProjectService;
import ru.t1.volkova.tm.api.service.IServiceLocator;
import ru.t1.volkova.tm.dto.request.project.*;
import ru.t1.volkova.tm.dto.response.project.*;
import ru.t1.volkova.tm.enumerated.ProjectSort;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;
import java.util.List;

@WebService(endpointInterface = "ru.t1.volkova.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndPoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    public IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String id = request.getId();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final Integer index = request.getIndex();
        @Nullable final String userId = session.getUserId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Project project = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        getProjectService().removeAll(userId);
        @Nullable List<Project> projects = getProjectService().findAll(userId);
        return new ProjectClearResponse(projects);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectGetByIdResponse getProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectGetByIdRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        return new ProjectGetByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectGetByIndexResponse getProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectGetByIndexRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().findOneByIndex(userId, index);
        return new ProjectGetByIndexResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectListRequest request
    ) throws Exception {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final ProjectSort sort = request.getSort();
        @Nullable final List<Project> projects;
        if (sort == null) projects = getProjectService().findAll(userId);
        else projects = getProjectService().findAll(userId, sort.getComparator());
        return new ProjectListResponse(projects);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Project project = getProjectService().removeOneById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIndexRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Project project = getProjectService().removeOneByIndex(userId, index);
        return new ProjectRemoveByIndexResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @Override
    @NotNull
    @WebMethod
    public ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIndexRequest request
    ) throws SQLException {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Project project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }

}
