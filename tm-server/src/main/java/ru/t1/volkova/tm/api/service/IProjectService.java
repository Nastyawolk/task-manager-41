package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.model.Project;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

@SuppressWarnings("UnusedReturnValue")
public interface IProjectService {

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description) throws SQLException;

    @NotNull
    Project updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description) throws SQLException;

    @NotNull
    Project changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws SQLException;

    @NotNull
    Project changeProjectStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    ) throws SQLException;

    @Nullable
    Project create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws SQLException;

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @Nullable
    Project findOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    @Nullable
    List<Project> findAll(@Nullable String userId) throws SQLException;

    @Nullable
    List<Project> findAlls() throws SQLException;

    @Nullable
    List<Project> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception;

    @Nullable
    List<Project> findAllByName(@Nullable String userId) throws SQLException;

    @Nullable
    List<Project> findAllByStatus(@Nullable String userId) throws SQLException;

    @Nullable
    List<Project> findAllByCreated(@Nullable String userId) throws SQLException;

    int getSize(@Nullable String userId) throws SQLException;

    @Nullable
    Project removeOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @Nullable
    Project removeOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    @NotNull
    Project removeOne(@Nullable String userId, @NotNull Project model) throws SQLException;

    void removeAll(@Nullable String userId) throws SQLException;

    @NotNull
    Project add(@NotNull Project model) throws SQLException;

}
