package ru.t1.volkova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.model.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (id, login, password, email, first_name, last_name, middle_name," +
            " locked, role) VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}," +
            " #{lastName}, #{middleName}, #{locked}, #{role})")
    int add(@NotNull User user) throws SQLException;

    @Update("UPDATE tm_user SET first_name = #{firstName}, last_name = #{lastName}," +
            " middle_name = #{middleName}, password = #{passwordHash}, locked = #{locked} WHERE id = #{id}")
    void update(@NotNull User user) throws SQLException;

    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password"),
    })
    @Nullable
    List<User> findAll() throws SQLException;

    @Select("SELECT * FROM tm_user WHERE id = #{id}")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password"),
    })
    @Nullable
    User findOneById(@Nullable @Param("id") String id) throws SQLException;

    @Select("SELECT COUNT(*) FROM tm_user")
    int getSize() throws SQLException;

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeOne(@NotNull User user) throws SQLException;

    @Delete("DELETE FROM tm_user")
    void removeAll() throws SQLException;

    @Select("SELECT * FROM tm_user WHERE login = #{login}")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password"),
    })
    @Nullable
    User findOneByLogin(@NotNull @Param("login") String login) throws SQLException;

    @Select("SELECT * FROM tm_user WHERE email = #{email}")
    @Results(value = {
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name"),
            @Result(property = "passwordHash", column = "password"),
    })
    @Nullable
    User findOneByEmail(@NotNull @Param("email") String email) throws SQLException;

}
