package ru.t1.volkova.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.repository.ITaskRepository;
import ru.t1.volkova.tm.api.service.IConnectionService;
import ru.t1.volkova.tm.api.service.ITaskService;
import ru.t1.volkova.tm.comparator.CreatedComparator;
import ru.t1.volkova.tm.comparator.NameComparator;
import ru.t1.volkova.tm.comparator.StatusComparator;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.entity.TaskNotFoundException;
import ru.t1.volkova.tm.exception.field.*;
import ru.t1.volkova.tm.model.Task;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return task;
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws SQLException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllByProjectId(userId, projectId);
        }
    }

    @Nullable
    @Override
    public List<Task> findAlls() throws SQLException {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAlls();
        }
    }

    @NotNull
    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final Task task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            task.setDescription(description);
            repository.update(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull Exception e) {
            sqlSession.rollback();
            throw e;
        }
    }

    @NotNull
    @Override
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws SQLException {
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final Task task = repository.findOneByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            task.setDescription(description);
            repository.update(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull Exception e) {
            sqlSession.rollback();
            throw e;
        }
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @NotNull final Status status
    ) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final Task task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setStatus(status);
            repository.update(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull Exception e) {
            sqlSession.rollback();
            throw e;
        }
    }

    @NotNull
    @Override
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @NotNull final Status status) throws SQLException {
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final Task task = repository.findOneByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            task.setStatus(status);
            repository.update(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull Exception e) {
            sqlSession.rollback();
            throw e;
        }
    }

    @NotNull
    @Override
    public Task findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final Task task = repository.findOneById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            return task;
        }
    }

    @Override
    @NotNull
    public Task findOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            @Nullable final Task task = repository.findOneByIndex(userId, index);
            if (task == null) throw new ProjectNotFoundException();
            return task;
        }
    }

    @Override
    @NotNull
    public Task add(@NotNull final Task task) throws SQLException {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.add(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return task;
    }

    @Override
    @Nullable
    public List<Task> findAll(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAll(userId);
        }
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId,
                              @Nullable final Comparator comparator
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            if (comparator == CreatedComparator.INSTANSE) return repository.findAllByCreated(userId);
            if (comparator == StatusComparator.INSTANSE) return repository.findAllByStatus(userId);
            if (comparator == NameComparator.INSTANSE) return repository.findAllByName(userId);
            return findAll(userId);
        }
    }

    @Nullable
    @Override
    public List<Task> findAllByName(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllByName(userId);
        }
    }

    @Override
    @Nullable
    public List<Task> findAllByStatus(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllByStatus(userId);
        }
    }

    @Override
    @Nullable
    public List<Task> findAllByCreated(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.findAllByCreated(userId);
        }
    }

    @Override
    public int getSize(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            return repository.getSize(userId);
        }
    }

    @Override
    @Nullable
    public Task removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws SQLException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Task newTask;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            newTask = repository.findOneById(userId, id);
            repository.removeOneById(userId, id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return newTask;
    }

    @Override
    @Nullable
    public Task removeOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Task newTask;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            newTask = repository.findOneByIndex(userId, index);
            repository.removeOneByIndex(userId, index);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return newTask;
    }

    @Override
    @Nullable
    public Task removeOne(
            @Nullable final String userId,
            @NotNull final Task task
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final Task newTask;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            newTask = repository.findOneById(userId, task.getId());
            repository.removeOne(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
        return newTask;
    }

    @Override
    public void removeAll(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.removeAll(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
    }

    @Override
    public void update(@NotNull final Task task) throws SQLException {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ITaskRepository repository = sqlSession.getMapper(ITaskRepository.class);
            repository.update(task);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        }
    }

}
