package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.model.User;

import java.sql.SQLException;
import java.util.List;

@SuppressWarnings("UnusedReturnValue")
public interface IUserService {

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws SQLException;

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws SQLException;

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws SQLException;

    @Nullable List<User> findAll() throws SQLException;

    @Nullable
    User findByLogin(@Nullable String login) throws SQLException;

    @Nullable
    User findByEmail(@Nullable String email) throws SQLException;

    @NotNull
    User findOneById(@Nullable String id) throws SQLException;

    @NotNull
    User removeOne(@Nullable User model) throws Exception;

    @NotNull
    User removeByLogin(@Nullable String login) throws Exception;

    @NotNull
    User removeByEmail(@Nullable String email) throws Exception;

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws Exception;

    @NotNull
    Boolean isLoginExist(@Nullable String login) throws SQLException;

    @NotNull
    Boolean isEmailExist(@Nullable String email) throws SQLException;

    @NotNull
    User lockUserByLogin(@Nullable String login) throws SQLException;

    @NotNull
    User unlockUserByLogin(@Nullable String login) throws SQLException;

    int getSize() throws SQLException;
}
