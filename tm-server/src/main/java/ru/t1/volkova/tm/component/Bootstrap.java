package ru.t1.volkova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.api.endpoint.*;
import ru.t1.volkova.tm.api.service.*;
import ru.t1.volkova.tm.endpoint.*;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.service.*;
import ru.t1.volkova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Getter
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectService, taskService, connectionService);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    @Getter
    private final IUserService userService =
            new UserService(connectionService, projectService, taskService, propertyService);

    @NotNull
    @Getter
    private final ISessionService sessionService = new SessionService(connectionService);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ICalcEndpoint calcEndpoint = new CalcEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(calcEndpoint);
        registry(systemEndpoint);
        registry(authEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "0.0.0.0";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void initDemoData() throws SQLException {
        @Nullable final User userTest = userService.findByLogin("test");
        @Nullable final User userCustom = userService.findByLogin("user");
        @Nullable final User userAdmin = userService.findByLogin("admin");

        projectService.create(userTest.getId(), "PROJECT12345", "Project for TestUser");
        projectService.create(userTest.getId(), "PROJECT12", "Project 2 for TestUser");
        projectService.create(userCustom.getId(), "PROJECT444", "Project for CustomUser");
        projectService.create(userAdmin.getId(), "PROJECT123", "Project for Admin");
        projectService.create(userAdmin.getId(), "PROJECT1", "Project 2 for Admin");

        taskService.create(userTest.getId(), "TASK1234", "test task");
        taskService.create(userTest.getId(), "TASK12", "test task2");
        taskService.create(userCustom.getId(), "TASK", "test task");
        taskService.create(userCustom.getId(), "TASK5", "test task");
        taskService.create(userAdmin.getId(), "TASK12", "test task");
        taskService.create(userAdmin.getId(), "TASK1", "test task");
    }

    public void start() throws SQLException {
        initPID();
        if (userService.findByLogin("admin") == null) userService.create("admin", "admin", Role.ADMIN);
        if (userService.findByLogin("user") == null) userService.create("user", "user", Role.USUAL);
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    public void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

}
