package ru.t1.volkova.tm.api.service;

import ru.t1.volkova.tm.api.repository.IRepository;
import ru.t1.volkova.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
