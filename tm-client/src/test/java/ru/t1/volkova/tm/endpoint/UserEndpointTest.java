package ru.t1.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.volkova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.volkova.tm.api.endpoint.IUserEndpoint;
import ru.t1.volkova.tm.dto.request.user.*;
import ru.t1.volkova.tm.dto.response.user.UserRegistryResponse;
import ru.t1.volkova.tm.marker.SoapCategory;
import ru.t1.volkova.tm.model.User;

import java.sql.SQLException;

public class UserEndpointTest {

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstanse();

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstanse();

    @NotNull
    private final static String userLogin = "user";

    @NotNull
    private final static String userPassword = "user";

    @NotNull
    private final static String newUserLogin = "userNew";

    @NotNull
    private final static String newUserPassword = "userNew";

    @NotNull
    private final static String newUserEmail = "userNew@mail.ru";

    @Nullable
    private String token;

    @Test
    @Category(SoapCategory.class)
    public void testChangePassword() throws Exception {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest();
        requestLogin.setLogin(userLogin);
        requestLogin.setPassword(userPassword);
        token = authEndpoint.login(requestLogin).getToken();

        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(token);
        request.setPassword("newPassword");
        userEndpoint.changeUserPassword(request);

        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(token);
        authEndpoint.logout(requestLogout);
        requestLogin.setPassword(request.getPassword());

        Assert.assertTrue(authEndpoint.login(requestLogin).getSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUserLock() throws SQLException {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest();
        requestLogin.setLogin("admin");
        requestLogin.setPassword("admin");
        token = authEndpoint.login(requestLogin).getToken();

        @NotNull final UserLockRequest request = new UserLockRequest(token);
        request.setLogin(userLogin);
        @Nullable final User user = userEndpoint.lockUser(request).getUser();
        if (user == null) {
            return;
        }
        Assert.assertTrue(user.getLocked());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUserRegistry() throws SQLException {
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(token);
        request.setLogin(newUserLogin);
        request.setEmail(newUserEmail);
        request.setPassword(newUserPassword);
        @NotNull final UserRegistryResponse responseCreate = userEndpoint.registryUser(request);
        if (responseCreate.getUser() == null) {
            return;
        }
        Assert.assertEquals(newUserLogin, responseCreate.getUser().getLogin());
        Assert.assertEquals(newUserEmail, responseCreate.getUser().getEmail());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUserRemove() throws Exception {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest();
        requestLogin.setLogin("admin");
        requestLogin.setPassword("admin");
        token = authEndpoint.login(requestLogin).getToken();

        @NotNull final UserRemoveRequest request = new UserRemoveRequest(token);
        request.setLogin(newUserLogin);
        Assert.assertTrue(userEndpoint.removeUser(request).getSuccess());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUserUnlock() throws SQLException {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest();
        requestLogin.setLogin("admin");
        requestLogin.setPassword("admin");
        token = authEndpoint.login(requestLogin).getToken();

        @NotNull final UserLockRequest requestLock = new UserLockRequest(token);
        requestLock.setLogin(userLogin);
        userEndpoint.lockUser(requestLock);

        @NotNull final UserUnlockRequest requestUnlock = new UserUnlockRequest(token);
        requestUnlock.setLogin(userLogin);
        @Nullable final User user = userEndpoint.unlockUser((requestUnlock)).getUser();
        if (user == null) {
            return;
        }
        Assert.assertFalse(user.getLocked());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUserUpdateProfile() throws Exception {
        @NotNull final UserLoginRequest requestLogin = new UserLoginRequest();
        requestLogin.setLogin(userLogin);
        requestLogin.setPassword("newPassword");
        token = authEndpoint.login(requestLogin).getToken();

        @NotNull final String lastName = "newLastName";
        @NotNull final String firstName = "newFirstName";
        @NotNull final String middleName = "newMiddleName";
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(token);
        request.setLastName(lastName);
        request.setFirstName(firstName);
        request.setMiddleName(middleName);
        @Nullable final User user = userEndpoint.updateUserProfile(request).getUser();
        if (user == null) {
            return;
        }
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

}
